CFLAGS += -Wall -pedantic -std=c99 -D_BSD_SOURCE

all: read_firmware gen_firmware

clean:
	rm -f *.o $(HELPERS)

gen_firmware: gen_firmware.c crc32.c
read_firmware: read_firmware.c crc32.c
